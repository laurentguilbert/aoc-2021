use std::fs;
use std::path::Path;

fn get_fish_count(lifecycles: &Vec<u64>, days: u32) -> u64 {
    let mut lifecycles = lifecycles.clone();
    for _ in 0..days {
        let new_fish_count = lifecycles[0];
        lifecycles[0] = 0;
        lifecycles.rotate_left(1);
        lifecycles[6] += new_fish_count;
        lifecycles[8] = new_fish_count;
    }
    lifecycles.iter().sum()
}

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d6.txt")).unwrap();

    let mut lifecycles: Vec<u64> = vec![0; 9];
    for lifecycle in contents
        .split(",")
        .map(|number| number.parse::<usize>().unwrap())
    {
        lifecycles[lifecycle] += 1;
    }

    println!("p1: {}", get_fish_count(&lifecycles, 80));
    println!("p2: {}", get_fish_count(&lifecycles, 256));
}

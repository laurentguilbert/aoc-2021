use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d13.txt")).unwrap();

    let mut groups = contents.split("\n\n");
    let points_group = groups.next().unwrap();
    let folds_group = groups.next().unwrap();

    let points: Vec<(i32, i32)> = points_group
        .split("\n")
        .map(|line| {
            let mut point_group = line.split(",");
            let x: i32 = point_group.next().unwrap().parse().unwrap();
            let y: i32 = point_group.next().unwrap().parse().unwrap();
            (x, y)
        })
        .collect();

    let folds: Vec<(i32, i32)> = folds_group
        .split("\n")
        .map(|line| {
            let direction = line.chars().nth(11).unwrap();
            let position: i32 = line[13..].parse().unwrap();
            if direction == 'x' {
                (position, 0)
            } else {
                (0, position)
            }
        })
        .collect();

    println!("p1: {}", p1(&points, &folds));
    println!("p2: {}", p2(&points, &folds));
}

fn print_points(points: &Vec<(i32, i32)>) {
    let min_x = points.iter().map(|point| point.0).min().unwrap();
    let max_x = points.iter().map(|point| point.0).max().unwrap();
    let min_y = points.iter().map(|point| point.1).min().unwrap();
    let max_y = points.iter().map(|point| point.1).max().unwrap();

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if points.contains(&(x, y)) {
                print!("##")
            } else {
                print!("  ")
            }
        }
        println!();
    }

    println!("\n({} points)\n", points.len());
}

fn p1(points: &Vec<(i32, i32)>, folds: &Vec<(i32, i32)>) -> usize {
    let mut folded_points = points.clone();
    for (index, fold) in folds.iter().enumerate() {
        folded_points = folded_points.iter().fold(Vec::new(), |mut acc, point| {
            let folded_point;

            if (fold.0 == 0 && point.1 <= fold.1) || (fold.1 == 0 && point.0 <= fold.0) {
                folded_point = *point;
            } else {
                if fold.0 == 0 {
                    folded_point = (point.0, fold.1 - (point.1 - fold.1))
                } else {
                    folded_point = (fold.0 - (point.0 - fold.0), point.1)
                }
            }

            if !acc.contains(&folded_point) {
                acc.push(folded_point);
            }

            acc
        });
        if index == 0 {
            break;
        }
    }
    folded_points.len()
}

fn p2(points: &Vec<(i32, i32)>, folds: &Vec<(i32, i32)>) -> usize {
    let mut folded_points = points.clone();
    for fold in folds {
        folded_points = folded_points.iter().fold(Vec::new(), |mut acc, point| {
            let folded_point;

            if (fold.0 == 0 && point.1 <= fold.1) || (fold.1 == 0 && point.0 <= fold.0) {
                folded_point = *point;
            } else {
                if fold.0 == 0 {
                    folded_point = (point.0, fold.1 - (point.1 - fold.1))
                } else {
                    folded_point = (fold.0 - (point.0 - fold.0), point.1)
                }
            }

            if !acc.contains(&folded_point) {
                acc.push(folded_point);
            }

            acc
        });
    }
    print_points(&folded_points);
    folded_points.len()
}

use std::fs;
use std::path::Path;

fn main() {
    let contents =
        fs::read_to_string(Path::new("inputs/d3.txt")).expect("Failed to read input file");
    let rows: Vec<Vec<char>> = contents
        .lines()
        .map(|line| line.chars().collect())
        .collect();

    println!("p1 {}", p1(&rows));
    println!("p2 {}", p2(&rows));
}

fn p1(rows: &Vec<Vec<char>>) -> isize {
    let mut bit_counts: Vec<usize> = vec![0; 12];
    for row in rows {
        for (i, bit) in row.iter().enumerate() {
            if *bit == '1' {
                bit_counts[i] += 1;
            }
        }
    }
    let gamma_rate_binary: String = bit_counts
        .iter()
        .map::<char, _>(|bit_count| {
            if *bit_count > (&rows.len() / 2) {
                '1'
            } else {
                '0'
            }
        })
        .collect();
    let gamma_rate_decimal = isize::from_str_radix(&gamma_rate_binary, 2).unwrap();

    let epsilon_rate_binary: String = gamma_rate_binary
        .chars()
        .map(|byte| if byte == '1' { '0' } else { '1' })
        .collect();
    let epsilon_rate_decimal = isize::from_str_radix(&epsilon_rate_binary, 2).unwrap();

    return gamma_rate_decimal * epsilon_rate_decimal;
}

fn p2(rows: &Vec<Vec<char>>) -> isize {
    let oxygen_generator_rating_binary: Vec<char> = filter_by_most_common(rows).unwrap();
    let oxygen_generator_rating_string: String = oxygen_generator_rating_binary.iter().collect();
    let oxygen_generator_rating_decimal =
        isize::from_str_radix(&oxygen_generator_rating_string, 2).unwrap();

    let co2_scrubber_rating_binary: Vec<char> = filter_by_least_common(rows).unwrap();
    let co2_scrubber_rating_string: String = co2_scrubber_rating_binary.iter().collect();
    let co2_scrubber_rating_decimal =
        isize::from_str_radix(&co2_scrubber_rating_string, 2).unwrap();

    oxygen_generator_rating_decimal * co2_scrubber_rating_decimal
}

fn filter_by_most_common(rows: &Vec<Vec<char>>) -> Option<Vec<char>> {
    let mut filtered_rows: Vec<Vec<char>> = rows.clone();
    for col_index in 0..12 {
        let mut threshold = filtered_rows.len() / 2;
        if filtered_rows.len() % 2 != 0 {
            threshold += 1
        }
        let filter_bit: char = if filtered_rows
            .iter()
            .map(|row| row[col_index])
            .filter(|&bit| bit == '1')
            .count()
            >= threshold
        {
            '1'
        } else {
            '0'
        };
        filtered_rows = filtered_rows
            .into_iter()
            .filter(|row| row[col_index] == filter_bit)
            .collect();
        if filtered_rows.len() == 1 {
            return Some(filtered_rows[0].to_vec());
        }
    }
    return None;
}

fn filter_by_least_common(rows: &Vec<Vec<char>>) -> Option<Vec<char>> {
    let mut filtered_rows: Vec<Vec<char>> = rows.clone();
    for col_index in 0..12 {
        let filter_bit: char = if filtered_rows
            .iter()
            .map(|row| row[col_index])
            .filter(|&bit| bit == '0')
            .count()
            <= filtered_rows.len() / 2
        {
            '0'
        } else {
            '1'
        };
        filtered_rows = filtered_rows
            .into_iter()
            .filter(|row| row[col_index] == filter_bit)
            .collect();
        if filtered_rows.len() == 1 {
            return Some(filtered_rows[0].to_vec());
        }
    }
    return None;
}

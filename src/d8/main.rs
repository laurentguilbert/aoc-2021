use std::collections::HashMap;
use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d8.txt")).unwrap();

    println!("p1 {}", p1(&contents));
    println!("p2 {}", p2(&contents));
}

fn p1(contents: &String) -> usize {
    let mut count = 0;
    for line in contents.lines() {
        let mut groups = line.split(" | ");
        groups.next();
        let outputs: Vec<&str> = groups.next().unwrap().split_whitespace().collect();
        count += outputs
            .iter()
            .filter(|output| [2, 4, 3, 7].contains(&output.len()))
            .count();
    }
    count
}

fn contains_other(number_to_signal: &HashMap<u8, &str>, signal: &str, number: u8) -> bool {
    number_to_signal
        .get(&number)
        .unwrap()
        .chars()
        .all(|char| signal.contains(char))
}

fn p2(contents: &String) -> u32 {
    let mut count = 0;
    for line in contents.lines() {
        let mut groups = line.split(" | ");

        let mut signals: Vec<&str> = groups.next().unwrap().split_whitespace().collect();
        signals.sort_by(|&a, &b| a.len().cmp(&b.len()));

        let outputs: Vec<&str> = groups.next().unwrap().split_whitespace().collect();

        let sorted_outputs: Vec<String> = outputs
            .iter()
            .map(|&output| {
                let mut sorted_output: Vec<char> = output.chars().collect();
                sorted_output.sort_by(|a, b| a.cmp(b));
                String::from_iter(sorted_output)
            })
            .collect();

        let mut number_to_signal: HashMap<u8, &str> = HashMap::new();
        number_to_signal.insert(1, signals[0]);
        number_to_signal.insert(4, signals[2]);
        number_to_signal.insert(7, signals[1]);
        number_to_signal.insert(8, signals[9]);

        for &signal in (&signals).iter().filter(|&signal| signal.len() == 5) {
            if contains_other(&number_to_signal, signal, 1) {
                number_to_signal.insert(3, signal);
            } else if signal
                .chars()
                .into_iter()
                .filter(|char| number_to_signal.get(&4).unwrap().contains(*char))
                .count()
                == 2
            {
                number_to_signal.insert(2, signal);
            } else {
                number_to_signal.insert(5, signal);
            }
        }
        for &signal in (&signals).into_iter().filter(|&signal| signal.len() == 6) {
            if contains_other(&number_to_signal, signal, 3) {
                number_to_signal.insert(9, signal);
            } else if contains_other(&number_to_signal, signal, 7) {
                number_to_signal.insert(0, signal);
            } else {
                number_to_signal.insert(6, signal);
            }
        }

        let mut signal_to_number: HashMap<String, u8> = HashMap::new();
        for (number, signal) in &number_to_signal {
            let mut sorted_signal: Vec<char> = signal.chars().collect();
            sorted_signal.sort_by(|a, b| a.cmp(b));
            let sorted_signal = String::from_iter(sorted_signal);

            signal_to_number.insert(sorted_signal, *number);
        }

        let output_numbers: Vec<u8> = sorted_outputs
            .iter()
            .map(|sorted_output| *signal_to_number.get(sorted_output).unwrap())
            .collect();
        let final_number: u32 = output_numbers
            .iter()
            .map(|number| number.to_string())
            .collect::<String>()
            .parse()
            .unwrap();
        count += final_number;
    }
    count
}

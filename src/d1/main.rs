use std::fs;
use std::path::Path;

fn main() {
    let contents =
        fs::read_to_string(Path::new("inputs/d1.txt")).expect("Failed to read input file");
    let readings: Vec<u32> = contents
        .lines()
        .map(|line| line.parse().expect("Failed to convert reading to integer"))
        .collect();
    println!("p1: {}", p1(&readings));
    println!("p2: {}", p2(&readings));
}

fn p1(readings: &Vec<u32>) -> u32 {
    let mut count = 0;
    let mut previous_reading: Option<u32> = None;

    for &reading in readings {
        match previous_reading {
            Some(previous_reading) => {
                if previous_reading < reading {
                    count += 1
                }
            }
            None => (),
        }
        previous_reading = Some(reading);
    }
    return count;
}

fn p2(readings: &Vec<u32>) -> u32 {
    let mut count = 0;
    let mut previous_sum: Option<u32> = None;

    for window in readings.windows(3) {
        let sum = window.iter().sum();
        match previous_sum {
            Some(previous_sum) => {
                if previous_sum < sum {
                    count += 1
                }
            }
            None => (),
        }
        previous_sum = Some(sum);
    }
    return count;
}

use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d10.txt")).unwrap();

    println!("p1 {}", p1(&contents));
    println!("p2 {}", p2(&contents));
}

fn get_score(char: char) -> u32 {
    match char {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => 0,
    }
}

fn p1(contents: &str) -> u32 {
    let mut result = 0;
    for line in contents.lines() {
        let mut stack: Vec<char> = Vec::new();
        for char in line.chars() {
            if ['(', '[', '{', '<'].contains(&char) {
                stack.insert(0, char)
            } else if let Some(last_open) = stack.first() {
                if (char == ')' && *last_open != '(')
                    || (char == ']' && *last_open != '[')
                    || (char == '}' && *last_open != '{')
                    || (char == '>' && *last_open != '<')
                {
                    result += get_score(char);
                    break;
                } else {
                    stack.remove(0);
                }
            } else {
                result += get_score(char);
                break;
            }
        }
    }
    result
}

fn p2(contents: &str) -> u64 {
    let mut line_scores: Vec<u64> = Vec::new();

    'line: for line in contents.lines() {
        let mut stack: Vec<char> = Vec::new();

        for char in line.chars() {
            if ['(', '[', '{', '<'].contains(&char) {
                stack.insert(0, char)
            } else if let Some(last_open_char) = stack.first() {
                if (char == ')' && *last_open_char != '(')
                    || (char == ']' && *last_open_char != '[')
                    || (char == '}' && *last_open_char != '{')
                    || (char == '>' && *last_open_char != '<')
                {
                    continue 'line;
                } else {
                    stack.remove(0);
                }
            } else {
                continue 'line;
            }
        }

        line_scores.push(stack.iter().fold(0, |acc, open_char| {
            let char_score = match open_char {
                '(' => 1,
                '[' => 2,
                '{' => 3,
                '<' => 4,
                _ => 0,
            };
            acc * 5 + char_score
        }));
    }

    line_scores.sort();
    line_scores[line_scores.len() / 2]
}

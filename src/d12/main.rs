use std::collections::HashMap;
use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d12.txt")).unwrap();

    let mut nodes: HashMap<String, Vec<String>> = HashMap::new();

    for line in contents.lines() {
        let mut line_split = line.split("-");

        let origin_node = line_split.next().unwrap().to_string();
        let dest_node = line_split.next().unwrap().to_string();

        let origin_children = nodes.entry(origin_node.clone()).or_insert(Vec::new());
        if !origin_children.contains(&dest_node) {
            origin_children.push(dest_node.clone());
        }

        let dest_children = nodes.entry(dest_node.clone()).or_insert(Vec::new());
        if !dest_children.contains(&origin_node) {
            dest_children.push(origin_node.clone());
        }
    }

    println!("p1 {}", p1(&nodes));
    println!("p2 {}", p2(&nodes));
}

fn find_paths_p1(
    node: &String,
    nodes: &HashMap<String, Vec<String>>,
    mut path: Vec<String>,
    paths: &mut Vec<Vec<String>>,
) -> () {
    path.push(node.to_string());
    if node == "end" {
        paths.push(path);
        return;
    }
    let children = nodes.get(node).unwrap();
    for child in children {
        let is_small_cave = child.chars().all(|c| c.is_ascii_lowercase());
        if path.contains(child) && is_small_cave {
            continue;
        }
        find_paths_p1(child, nodes, path.clone(), paths);
    }
}

fn p1(nodes: &HashMap<String, Vec<String>>) -> usize {
    let mut paths: Vec<Vec<String>> = Vec::new();
    find_paths_p1(&String::from("start"), nodes, Vec::new(), &mut paths);
    paths.len()
}

fn is_lowercase(str: &String) -> bool {
    str.chars().all(|c| c.is_ascii_lowercase())
}

fn find_paths_p2(
    node: &String,
    nodes: &HashMap<String, Vec<String>>,
    mut path: Vec<String>,
    paths: &mut Vec<Vec<String>>,
) -> () {
    path.push(node.to_string());
    if node == "end" {
        paths.push(path);
        return;
    }
    let children = nodes.get(node).unwrap();
    for child in children {
        if child == "start" {
            continue;
        }

        let is_small_cave = is_lowercase(child);
        let visited_small_cave_twice = path
            .iter()
            .filter(|node| is_lowercase(&node))
            .any(|node| path.iter().filter(|&path_node| path_node == node).count() >= 2);

        if is_small_cave && path.contains(child) && visited_small_cave_twice {
            continue;
        }

        find_paths_p2(child, nodes, path.clone(), paths);
    }
}

fn p2(nodes: &HashMap<String, Vec<String>>) -> usize {
    let mut paths: Vec<Vec<String>> = Vec::new();
    find_paths_p2(&String::from("start"), nodes, Vec::new(), &mut paths);
    paths.len()
}

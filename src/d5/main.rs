use std::collections::HashMap;
use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d5.txt")).unwrap();

    println!("p1: {}", p1(&contents));
    println!("p2: {}", p2(&contents));
}

fn p1(contents: &String) -> usize {
    let mut clouds: HashMap<(u32, u32), u32> = HashMap::new();
    for line in contents.lines() {
        let mut entry = line.split(" -> ");

        let mut origin_repr = entry.next().unwrap().split(",");
        let x1: u32 = origin_repr.next().unwrap().parse().unwrap();
        let y1: u32 = origin_repr.next().unwrap().parse().unwrap();

        let mut dest_repr = entry.next().unwrap().split(",");
        let x2: u32 = dest_repr.next().unwrap().parse().unwrap();
        let y2: u32 = dest_repr.next().unwrap().parse().unwrap();

        if y1 == y2 {
            let x_range = if x1 > x2 { x2..=x1 } else { x1..=x2 };
            for x in x_range {
                *clouds.entry((x, y1)).or_insert(0) += 1;
            }
        } else if x1 == x2 {
            let y_range = if y1 > y2 { y2..=y1 } else { y1..=y2 };
            for y in y_range {
                *clouds.entry((x1, y)).or_insert(0) += 1;
            }
        }
    }
    clouds.into_values().filter(|&count| count >= 2).count()
}

fn p2(contents: &String) -> usize {
    let mut clouds: HashMap<(u32, u32), u32> = HashMap::new();
    for line in contents.lines() {
        let mut entry = line.split(" -> ");

        let mut origin_repr = entry.next().unwrap().split(",");
        let x1: u32 = origin_repr.next().unwrap().parse().unwrap();
        let y1: u32 = origin_repr.next().unwrap().parse().unwrap();

        let mut dest_repr = entry.next().unwrap().split(",");
        let x2: u32 = dest_repr.next().unwrap().parse().unwrap();
        let y2: u32 = dest_repr.next().unwrap().parse().unwrap();

        if y1 == y2 {
            let x_range = if x1 > x2 { x2..=x1 } else { x1..=x2 };
            for x in x_range {
                *clouds.entry((x, y1)).or_insert(0) += 1;
            }
        } else if x1 == x2 {
            let y_range = if y1 > y2 { y2..=y1 } else { y1..=y2 };
            for y in y_range {
                *clouds.entry((x1, y)).or_insert(0) += 1;
            }
        } else {
            let x_range: Vec<u32> = if x1 > x2 {
                (x2..=x1).rev().collect()
            } else {
                (x1..=x2).collect()
            };
            let y_range: Vec<u32> = if y1 > y2 {
                (y2..=y1).rev().collect()
            } else {
                (y1..=y2).collect()
            };
            for coords in x_range.iter().zip(y_range.iter()) {
                let (x, y) = coords;
                *clouds.entry((*x, *y)).or_insert(0) += 1;
            }
        }
    }
    clouds.into_values().filter(|&count| count >= 2).count()
}

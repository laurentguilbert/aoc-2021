use std::fs;
use std::path::Path;

#[derive(Debug)]
struct Board {
    board: Vec<Vec<u32>>,
}

impl Board {
    fn is_winning(&self, numbers_drawn: &Vec<u32>) -> bool {
        let first_row = self.board.first().unwrap();
        for index in 0..first_row.len() {
            if self.board[index]
                .iter()
                .map(|number| numbers_drawn.contains(number))
                .all(|check| check == true)
                || self
                    .board
                    .iter()
                    .map(|row| row[index])
                    .map(|number| numbers_drawn.contains(&number))
                    .all(|check| check == true)
            {
                return true;
            }
        }
        false
    }

    fn get_score(&self, numbers_drawn: &Vec<u32>) -> u32 {
        let mut score = 0;
        for row in &self.board {
            for number in row {
                if !numbers_drawn.contains(&number) {
                    score += number;
                }
            }
        }
        score * numbers_drawn.last().unwrap()
    }
}

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d4.txt")).unwrap();
    let contents: Vec<&str> = contents.split("\n\n").collect();

    let numbers_drawn: Vec<u32> = contents[0]
        .split(",")
        .map(|number| number.parse().unwrap())
        .collect();

    let boards: Vec<Board> = contents
        .into_iter()
        .skip(1)
        .map(|board_content| Board {
            board: board_content
                .split("\n")
                .map(|board_row_content| {
                    board_row_content
                        .split_whitespace()
                        .map(|number| number.parse().unwrap())
                        .collect()
                })
                .collect(),
        })
        .collect();

    println!("p1: {}", p1(&numbers_drawn, &boards).unwrap());
    println!("p2: {}", p2(&numbers_drawn, &boards).unwrap());
}

fn p1(numbers_drawn: &Vec<u32>, boards: &Vec<Board>) -> Option<u32> {
    for index in 0..numbers_drawn.len() {
        let numbers_drawn_slice = numbers_drawn[..index].to_vec();
        for board in boards {
            if board.is_winning(&numbers_drawn_slice) {
                return Some(board.get_score(&numbers_drawn_slice));
            }
        }
    }
    None
}

fn p2(numbers_drawn: &Vec<u32>, boards: &Vec<Board>) -> Option<u32> {
    let mut loosing_boards: Vec<&Board> = boards.iter().collect();
    for index in 0..numbers_drawn.len() {
        let numbers_drawn_slice = numbers_drawn[..index].to_vec();
        if loosing_boards.len() == 1 && loosing_boards[0].is_winning(&numbers_drawn_slice) {
            return Some(loosing_boards[0].get_score(&numbers_drawn_slice));
        }
        loosing_boards = loosing_boards
            .into_iter()
            .filter(|&board| !board.is_winning(&numbers_drawn_slice))
            .collect();
    }
    None
}

use ansi_term::Colour::Red;
use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d11.txt")).unwrap();
    let octopuses: Vec<Vec<u32>> = contents
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_digit(10).unwrap())
                .collect()
        })
        .collect();

    println!("p1 {}", p1(&octopuses));
    println!("p2 {}", p2(&octopuses));
}

fn flash(
    x: usize,
    y: usize,
    octopuses: &mut Vec<Vec<u32>>,
    flashes: &mut Vec<(usize, usize)>,
) -> () {
    if flashes.contains(&(x, y)) {
        return;
    }
    octopuses[y][x] = 0;
    flashes.push((x, y));

    for y_offset in [-1, 0, 1] {
        let adjacent_y = y as i32 + y_offset;
        if adjacent_y < 0 || adjacent_y > octopuses.len() as i32 - 1 {
            continue;
        }

        for x_offset in [-1, 0, 1] {
            let adjacent_x = x as i32 + x_offset;
            if adjacent_x < 0
                || adjacent_x > octopuses[adjacent_y as usize].len() as i32 - 1
                || (y_offset == 0 && x_offset == 0)
            {
                continue;
            }

            let adjacent_x = adjacent_x as usize;
            let adjacent_y = adjacent_y as usize;

            if !flashes.contains(&(adjacent_x, adjacent_y)) {
                octopuses[adjacent_y][adjacent_x] += 1;
            }
            if octopuses[adjacent_y][adjacent_x] > 9 {
                flash(adjacent_x, adjacent_y, octopuses, flashes)
            }
        }
    }
}

fn p1(octopuses: &Vec<Vec<u32>>) -> u64 {
    let mut octopuses = octopuses.clone();
    let mut result = 0;

    for step in 1..=100 {
        let mut flashes: Vec<(usize, usize)> = Vec::new();

        for row in octopuses.iter_mut() {
            for octopus in row.iter_mut() {
                *octopus += 1
            }
        }

        let mut loaded_octopuses: Vec<(usize, usize)> = Vec::new();
        for (y, row) in octopuses.iter().enumerate() {
            for (x, octopus) in row.iter().enumerate() {
                if *octopus > 9 {
                    loaded_octopuses.push((x, y));
                }
            }
        }

        for (x, y) in loaded_octopuses {
            flash(x, y, &mut octopuses, &mut flashes);
        }

        result += flashes.len() as u64;

        println!("----------\nStep {}\n----------", step);
        for (y, row) in octopuses.iter().enumerate() {
            for (x, octopus) in row.iter().enumerate() {
                if flashes.contains(&(x, y)) {
                    print!("{}", Red.paint(octopus.to_string()));
                } else {
                    print!("{}", octopus);
                }
            }
            println!();
        }
        println!();
    }

    result
}

fn p2(octopuses: &Vec<Vec<u32>>) -> u64 {
    let mut octopuses = octopuses.clone();

    let mut step_count = 0;
    loop {
        step_count += 1;

        let mut flashes: Vec<(usize, usize)> = Vec::new();

        for row in octopuses.iter_mut() {
            for octopus in row.iter_mut() {
                *octopus += 1
            }
        }

        let mut loaded_octopuses: Vec<(usize, usize)> = Vec::new();
        for (y, row) in octopuses.iter().enumerate() {
            for (x, octopus) in row.iter().enumerate() {
                if *octopus > 9 {
                    loaded_octopuses.push((x, y));
                }
            }
        }

        for (x, y) in loaded_octopuses {
            flash(x, y, &mut octopuses, &mut flashes);
        }

        if flashes.len() == 100 {
            return step_count;
        }
    }
}

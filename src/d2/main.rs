use std::fs;
use std::path::Path;

fn main() {
    let contents =
        fs::read_to_string(Path::new("inputs/d2.txt")).expect("Failed to read input file");
    let instructions: Vec<(&str, u32)> = contents
        .lines()
        .map(|line| {
            let splitted: Vec<&str> = line.split_whitespace().collect();
            let dir: &str = splitted[0];
            let dist: u32 = splitted[1].parse().unwrap();
            (dir, dist)
        })
        .collect();

    println!("p1: {}", p1(&instructions));
    println!("p2: {}", p2(&instructions));
}

fn p1(instructions: &Vec<(&str, u32)>) -> u32 {
    let mut depth = 0;
    let mut horizontal = 0;
    for &(dir, dist) in instructions {
        match dir {
            "forward" => horizontal += dist,
            "down" => depth += dist,
            "up" => depth -= dist,
            _ => (),
        }
    }
    return depth * horizontal;
}

fn p2(instructions: &Vec<(&str, u32)>) -> u32 {
    let mut depth = 0;
    let mut aim = 0;
    let mut horizontal = 0;
    for &(dir, dist) in instructions {
        match dir {
            "forward" => {
                horizontal += dist;
                depth += dist * aim;
            }
            "down" => aim += dist,
            "up" => aim -= dist,
            _ => (),
        }
    }
    return depth * horizontal;
}

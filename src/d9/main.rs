use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d9.txt")).unwrap();

    println!("p1 {}", p1(&contents));
    println!("p2 {}", p2(&contents));
}

fn p1(contents: &str) -> u32 {
    let heightmap: Vec<Vec<u32>> = contents
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_digit(10).unwrap())
                .collect::<Vec<u32>>()
        })
        .collect();

    let mut count = 0;
    for (y, row) in heightmap.iter().enumerate() {
        for (x, &height) in row.iter().enumerate() {
            if (y == 0 || heightmap[y - 1][x] > height)
                && (y == heightmap.len() - 1 || heightmap[y + 1][x] > height)
                && (x == 0 || heightmap[y][x - 1] > height)
                && (x == row.len() - 1 || heightmap[y][x + 1] > height)
            {
                count += heightmap[y][x] + 1
            }
        }
    }
    count
}

fn find_basin_recur(
    heightmap: &Vec<Vec<u32>>,
    x: usize,
    y: usize,
    basin: &mut Vec<(usize, usize)>,
) -> () {
    let height = heightmap[y][x];
    if basin.contains(&(x, y)) || height == 9 {
        return;
    }
    basin.push((x, y));

    if y > 0 {
        find_basin_recur(heightmap, x, y - 1, basin);
    }
    if y < heightmap.len() - 1 {
        find_basin_recur(heightmap, x, y + 1, basin);
    }
    if x > 0 {
        find_basin_recur(heightmap, x - 1, y, basin);
    }
    if x < heightmap[y].len() - 1 {
        find_basin_recur(heightmap, x + 1, y, basin);
    }
}

fn p2(contents: &str) -> usize {
    let heightmap: Vec<Vec<u32>> = contents
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_digit(10).unwrap())
                .collect::<Vec<u32>>()
        })
        .collect();

    let mut basins: Vec<Vec<(usize, usize)>> = Vec::new();
    for (y, row) in heightmap.iter().enumerate() {
        for (x, &height) in row.iter().enumerate() {
            if (y == 0 || heightmap[y - 1][x] > height)
                && (y == heightmap.len() - 1 || heightmap[y + 1][x] > height)
                && (x == 0 || heightmap[y][x - 1] > height)
                && (x == row.len() - 1 || heightmap[y][x + 1] > height)
            {
                let mut basin: Vec<(usize, usize)> = Vec::new();
                find_basin_recur(&heightmap, x, y, &mut basin);
                basins.push(basin);
            }
        }
    }

    basins.sort_by(|a, b| b.len().cmp(&a.len()));
    basins[0..3]
        .iter()
        .map(|basin| basin.len())
        .reduce(|a, b| a * b)
        .unwrap()
}

use core::cmp::max;
use core::cmp::min;
use std::fs;
use std::path::Path;

fn main() {
    let contents = fs::read_to_string(Path::new("inputs/d7.txt")).unwrap();

    let crab_positions: Vec<u32> = contents
        .split(",")
        .map(|number| number.parse().unwrap())
        .collect();
    let min_pos = crab_positions.iter().min().unwrap();
    let max_pos = crab_positions.iter().max().unwrap();

    println!("p1 {}", p1(&crab_positions, min_pos, max_pos).unwrap());
    println!("p2 {}", p2(&crab_positions, min_pos, max_pos).unwrap());
}

fn p1(crab_positions: &Vec<u32>, min_pos: &u32, max_pos: &u32) -> Option<u32> {
    let mut min_fuel: Option<u32> = None;
    for pos in *min_pos..*max_pos {
        let pos_fuel: u32 = crab_positions
            .iter()
            .map(|crab_pos| max(crab_pos, &pos) - min(crab_pos, &pos))
            .sum();
        min_fuel = match min_fuel {
            Some(min_fuel) => {
                if pos_fuel < min_fuel {
                    Some(pos_fuel)
                } else {
                    Some(min_fuel)
                }
            }
            None => Some(pos_fuel),
        };
    }
    min_fuel
}

fn p2(crab_positions: &Vec<u32>, min_pos: &u32, max_pos: &u32) -> Option<u32> {
    let mut min_fuel: Option<u32> = None;
    for pos in *min_pos..*max_pos {
        let pos_fuel: u32 = crab_positions
            .iter()
            .map::<u32, _>(|crab_pos| {
                let offset = max(crab_pos, &pos) - min(crab_pos, &pos);
                offset * (offset + 1) / 2
            })
            .sum();
        min_fuel = match min_fuel {
            Some(min_fuel) => {
                if pos_fuel < min_fuel {
                    Some(pos_fuel)
                } else {
                    Some(min_fuel)
                }
            }
            None => Some(pos_fuel),
        };
    }
    min_fuel
}
